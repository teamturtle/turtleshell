package turtle.mod.tshell;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.network.NetworkMod;
import turtle.mod.tshell.lib.CommonProxy;
import turtle.mod.tshell.lib.PacketHandler;

/**
 * Created by Louis on 06/01/14.
 */
@Mod(modid = "TShell", name = "TurtleShell", version = "0.1.1")
@NetworkMod(channels = {"TShell"}, clientSideRequired = true, serverSideRequired = false, packetHandler = PacketHandler.class)
public class TurtleShell {

    @Mod.Instance("TShell")
    public static TurtleShell instance;

    @SidedProxy(clientSide = "turtle.mod.tshell.lib.ClientProxy", serverSide = "turtle.mod.tshell.lib.CommonProxy")
    public static CommonProxy proxy;

}